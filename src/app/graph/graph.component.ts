import { Component, OnInit } from '@angular/core';
import { GraphFactory } from '../GraphFactory.model';
import * as CanvasJS from '../../assets/canvasjs.min';
import { timeout } from 'q';
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})

export class GraphComponent implements OnInit {
  public dataCame=false;
  
  lineChartData
   lineChartLabels
  
    
   



  graphObj = new GraphFactory();

  constructor() {}

  



  ngOnInit() {
     
    this.graphObj.getGraphdata.then((gdata) => {
      //   console.log("came data")
     var yValues=[]
     var xValues=[]
      for (let id in gdata) {
       // console.log(gdata[id]["y"])
       yValues.push(gdata[id]["y"])
      xValues.push(gdata[id]["label"])
      }
    
     //   console.log(yValues)
      this.lineChartData = [
       { data: yValues, label: 'Ideal Line' },
       { data: [], label: 'Actual Line' }
       // {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
     ];
    this.lineChartLabels = xValues;
   this.dataCame=true
       });
   
   }


//make the graph
public lineChartOptions= {
  responsive: true,
  // title: {
  //   display: true,
  //   text: 'Embla Burndown Chart'
  // },
  legend: {
    display: true,
    position: 'right',
    labels: {
      fontColor: 'black',
      fontSize: 14

    }
  },

  scales: {
    yAxes: [{

      gridLines: {
        display: false,
        color:'rgb(0, 0, 0)',
        lineWidth:2,
        
        
      },
      scaleLabel: {
        display: true,
        labelString: 'REMAINING TIME ESTIMATE',
        fontSize: 14
      }


    }],
    xAxes: [{

      gridLines: {
        display: false,
        color:'rgb(0, 0, 0)',
        lineWidth:2
      },
      scaleLabel: {
        display: true,
        labelString: 'TIME',
        fontSize: 14
      }

    }]

  }



};
public lineChartColors = [
  { // grey
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(128,128,128,1)',
    //pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  },
  { // red
    backgroundColor: 'rgba(77,83,96,0.2)',
    borderColor: 'rgba(255,0,0,1)',
    //pointBackgroundColor: 'rgba(255,255,255,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(77,83,96,1)'
  },
  // { // grey
  //   backgroundColor: 'rgba(148,159,177,0.2)',
  //   borderColor: 'rgba(148,159,177,1)',
  //   pointBackgroundColor: 'rgba(148,159,177,1)',
  //   pointBorderColor: '#fff',
  //   pointHoverBackgroundColor: '#fff',
  //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  // }
];

public lineChartLegend=true;
public lineChartType = 'line';

// public randomize():void {
//   let _lineChartData:Array<any> = new Array(this.lineChartData.length);
//   for (let i = 0; i < this.lineChartData.length; i++) {
//     _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
//     for (let j = 0; j < this.lineChartData[i].data.length; j++) {
//       _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
//     }
//   }
//   this.lineChartData = _lineChartData;
// }

// events

}


