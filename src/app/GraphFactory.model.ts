//import { Graph } from "./graph/graph.component";
//sandeerads edited
//edited after getting admin

declare var test: any;
var gloGraph;


export class GraphFactory {


  //make a promise as this is async request
  getGraphdata = new Promise((resolve, reject) => {

    var sprintsData = []

var AP=test();
    AP.getLocation((location) => {



      // Get the url location
      AP.request('/rest/agile/1.0/board', {
        contentType: 'application/json',
        success: function (dataj) {
          // prase json object to js object
          var data = JSON.parse(dataj);
          //get relavent agile board for project 
          var proKey = new URL(location).searchParams.get("project.key");






          var viewId = data["values"].find(x => x.location.projectKey == proKey)

          // console.log(viewId);
          var rapidViewId = viewId["id"]



          //second call get data of sprints for related board

          AP.request('/rest/agile/1.0/board/' + rapidViewId + '/sprint', {
            contentType: 'application/json',
            success: function (dataj) {
              //  console.log(dataj);
              var data = JSON.parse(dataj);

              var activeSprintId;

              for (let id in data["values"]) {

                //get the active sprint
                if (data["values"][id]["state"] == "active") { activeSprintId = data['values'][id]["id"] }
                // code to get  sprints of project
                sprintsData.push({
                  sprintId: data['values'][id]["id"],
                  sprintName: data['values'][id]["name"]
                });

              }

              //  console.log(sprintsData);

              //after get the current sprint and load its data 67 as def sprint

             AP.request('/rest/agile/1.0/sprint/' + activeSprintId, {
                contentType: 'application/json',
                success: function (dataj) {
                  //  console.log(dataj);
                  var data = JSON.parse(dataj);

                  //get dtart and end dates of sprint
                  var startDate = data['startDate']
                  var endDate = data['endDate']
              
                  AP.request('/rest/agile/1.0/board/' + rapidViewId + '/sprint/' + activeSprintId + '/issue?fields=timetracking,created,summary&maxResults=100', {
                    contentType: 'application/json',
                    success: function (dataj) {
                      //  console.log(dataj);
                      var data = JSON.parse(dataj);

                      //get sum of hours for relavent sprint

                      var startDateUtc = new Date(startDate)
                      var endDateUtc = new Date(endDate)
                     // console.log("start date :  "+startDateUtc)
                      // console.log("end date :  "+endDateUtc)
                      var sumHours = 0;
                      for (let i = 0; i < data['issues'].length; i++) {

                        var timeSecondStr = data['issues'][i]['fields']['timetracking']['originalEstimateSeconds']
                        var createDate = data['issues'][i]['fields']['created']
                        var sum = data['issues'][i]['fields']['summary']

                        var timeSeconds = Number(timeSecondStr)
                        // var s=parseInt(timeSecondStr)
                      //  console.log( createDate)
                      //  console.log(sum)
                       // console.log(timeSecondStr/3600)
                        var createDateUtc = new Date(createDate)

                        if ((!(timeSecondStr === undefined)) && (startDateUtc >= createDateUtc)) {
                          sumHours = (timeSeconds / 3600) + sumHours
                        //  console.log("selected tasks " +sum)
                        //   console.log(timeSeconds/3600)
                        //  console.log( "selectedcreate day "+createDate)
                        }

                      }

                     // console.log(sumHours)


                      //edit data to make graph required format


                      const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June",
                        "July", "Aug", "Sep", "Oct", "Nov", "Dec"
                      ];
                      var timeDiff = Math.abs(startDateUtc.getTime() - endDateUtc.getTime());
                      var dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    //  console.log(dayDifference);
                      // for(var i=0;i<data['workingDaysConfig']['nonWorkingDays'].length;i++){
                      // console.log(data['workingDaysConfig']['nonWorkingDays'][i])
                      var dataPoints = [];



                    
                      var ave = sumHours / (dayDifference + 1)


                      for (var j = 0; j <= dayDifference + 1; j++) {
                        var day = new Date(startDateUtc)
                      //  console.log(startDateUtc.getDate());
                        day.setDate(startDateUtc.getDate() + j)
                      //  console.log(day)
                        if (j < dayDifference + 1) {
                          // console.log( monthNames[day.getMonth()]+" "+(day.getDate()))
                          //  console.log(ave*(dayDifference+1-j))
                          dataPoints.push({
                            label: monthNames[day.getMonth()] + " " + (day.getDate()),
                            y: (ave * (dayDifference + 1 - j))

                          });


                        }
                        else {
                          dataPoints.push({
                            label: monthNames[day.getMonth()] + " " + (day.getDate()),
                            y: 0
                          });

                        }

                      }
                    //  console.log("datapoint check")

                      gloGraph = dataPoints
                    //  console.log(gloGraph)
                      resolve(gloGraph);

                    },
                    error: function (xhr, statusText, errorThrown) {
                      console.log(arguments);
                    }
                  });




                },
                error: function (xhr, statusText, errorThrown) {
                  console.log(arguments);
                }
              });


            },
            error: function (xhr, statusText, errorThrown) {
              console.log(arguments);
            }
          });





          console.log("worked");




        },
        error: function (xhr, statusText, errorThrown) {
          console.log(arguments);
        }
      }); //


    });

  });

}